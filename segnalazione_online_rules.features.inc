<?php

/**
 * Implementation of hook_rules_defaults().
 */
function segnalazione_online_rules_rules_defaults() {
  return array(
    'rules' => array(
      'segnalazione_online_rules_1' => array(
        '#type' => 'rule',
        '#set' => 'event_node_insert',
        '#label' => 'Avviso di ricevimenti richiesta',
        '#active' => 0,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'richieste',
          'segnalazione_online_rules' => 'segnalazione_online_rules',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#type' => 'condition',
            '#settings' => array(
              'type' => array(
                'richiesta' => 'richiesta',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#name' => 'rules_condition_content_is_type',
            '#info' => array(
              'label' => 'Creazione di un contenuto richiesta',
              'label callback' => FALSE,
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Contenuto',
                ),
              ),
              'module' => 'Node',
            ),
            '#weight' => 0,
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Manda una mail all\'autore della richiesta',
              'label callback' => FALSE,
              'arguments' => array(
                'user' => array(
                  'type' => 'user',
                  'label' => 'Recipient',
                ),
              ),
              'module' => 'System',
              'eval input' => array(
                '0' => 'subject',
                '1' => 'message',
                '2' => 'from',
              ),
            ),
            '#name' => 'rules_action_mail_to_user',
            '#settings' => array(
              'from' => '',
              'subject' => '[Sardegna Consumatore] Richiesta inviata correttamente',
              'message' => 'Gentile [author:user],
La Sua richiesta è stata correttamente inviata agli operatori delle associazioni che partecipano al progetto.  Gli operatori elaboreranno una risposta che verrà inviata all’ indirizzo email da lei registrata nel nostro portale. 
Distinti saluti
--
http://www.sardegnaconsumatore.it

',
              '#argument map' => array(
                'user' => 'author',
              ),
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'message' => array(
                    '0' => 'author',
                  ),
                  'from' => array(
                    '0' => 'author',
                  ),
                ),
              ),
            ),
            '#type' => 'action',
          ),
          '1' => array(
            '#info' => array(
              'label' => 'Mostra un messaggio a video per l\'utente',
              'label callback' => FALSE,
              'module' => 'System',
              'eval input' => array(
                '0' => 'message',
              ),
            ),
            '#name' => 'rules_action_drupal_message',
            '#settings' => array(
              'message' => 'Gentile [author:user],
La Sua richiesta è stata correttamente inviata agli operatori delle associazioni che partecipano al progetto.  Gli operatori elaboreranno una risposta che verrà inviata all’ indirizzo email da lei utilizzata per la registrazione nel nostro portale.
',
              'error' => 0,
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'message' => array(
                    '0' => 'author',
                  ),
                ),
              ),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
          '3' => array(
            '#info' => array(
              'label' => 'Manda una mail a tutti gli utenti "operatore"',
              'label callback' => FALSE,
              'module' => 'System',
              'eval input' => array(
                '0' => 'subject',
                '1' => 'message',
                '2' => 'from',
              ),
            ),
            '#name' => 'rules_action_mail_to_users_of_role',
            '#settings' => array(
              'recipients' => array(
                '0' => 12,
                '1' => 4,
              ),
              'from' => '',
              'subject' => '[SITCR] Nuova richiesta [node:title]',
              'message' => 'Gentile Operatore/Operatrice
Una nuova richiesta è stata inoltrata dall\'utente [author:user] tramite il sito SardegnaConsumatore. 
Si ricorda che il Community manager attribuirà a rotazione ad una delle quattro associazioni il compito di evadere la richiesta nell’arco di massimo 48 ore lavorative. 

Se dovesse essere attribuita alla tua associazione allora segui la procedura di risposta come da Linee guida e partecipa nel forum della tua associazione per elaborare la risposta.
Il testo della richiesta pervenuta è il seguente:
###
[node:node-body]
###
Se hai bisogno di supporto per comporre la risposta puoi contattare SardegnaIT al n° 070 6069040
--
http://www.sardegnaconsumatore.it
',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'subject' => array(
                    '0' => 'node',
                  ),
                  'message' => array(
                    '0' => 'node',
                    '1' => 'author',
                  ),
                ),
              ),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'segnalazione_online_rules_2' => array(
        '#type' => 'rule',
        '#set' => 'event_workflow_state_changed',
        '#label' => 'Risposta inviata',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Attribuzione automatica',
          'segnalazione_online_rules' => 'segnalazione_online_rules',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#type' => 'condition',
            '#settings' => array(
              'type' => array(
                'risposta' => 'risposta',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#name' => 'rules_condition_content_is_type',
            '#info' => array(
              'label' => 'Updated content is Risposta',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Contenuto',
                ),
              ),
              'module' => 'Node',
            ),
            '#weight' => 0,
          ),
          '1' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Check workflow transition from Da validare to Inviata',
              'arguments' => array(
                'old_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'Old workflow state',
                ),
                'new_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'New workflow state',
                ),
              ),
              'module' => 'Workflow',
            ),
            '#name' => 'workflow_check_transition',
            '#settings' => array(
              'from_state' => array(
                '15' => '15',
              ),
              'to_state' => array(
                '16' => '16',
              ),
              '#argument map' => array(
                'old_state' => 'old_state',
                'new_state' => 'new_state',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#settings' => array(
              'code' => 'attribuzione_automatica_complete($node->nid);',
              'vars' => array(
                '0' => 'node',
              ),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'segnalazione_online_rules_3' => array(
        '#type' => 'rule',
        '#set' => 'event_workflow_state_changed',
        '#label' => 'Eliminazione alert',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Attribuzione automatica',
          'segnalazione_online_rules' => 'segnalazione_online_rules',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Updated content is Risposta',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Contenuto',
                ),
              ),
              'module' => 'Node',
            ),
            '#name' => 'rules_condition_content_is_type',
            '#settings' => array(
              'type' => array(
                'risposta' => 'risposta',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#type' => 'condition',
          ),
          '1' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Check workflow transition from (creation), In elaborazione, Da validare to Inviata',
              'arguments' => array(
                'old_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'Old workflow state',
                ),
                'new_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'New workflow state',
                ),
              ),
              'module' => 'Workflow',
            ),
            '#name' => 'workflow_check_transition',
            '#settings' => array(
              'from_state' => array(
                '13' => '13',
                '14' => '14',
                '15' => '15',
              ),
              'to_state' => array(
                '16' => '16',
              ),
              '#argument map' => array(
                'old_state' => 'old_state',
                'new_state' => 'new_state',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => 'attribuzione_automatica_remove_association_risposta($node->nid);',
              'vars' => array(
                '0' => 'node',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'segnalazione_online_rules_4' => array(
        '#type' => 'rule',
        '#set' => 'event_node_insert',
        '#label' => 'Invio email assegnazione automatica',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Attribuzione automatica',
          'segnalazione_online_rules' => 'segnalazione_online_rules',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#type' => 'condition',
            '#settings' => array(
              'type' => array(
                'richiesta' => 'richiesta',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#name' => 'rules_condition_content_is_type',
            '#info' => array(
              'label' => 'Created content is Richiesta',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Contenuto',
                ),
              ),
              'module' => 'Node',
            ),
            '#weight' => 0,
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'code' => 'gestione_richiesta_email_operatori_richiesta($node->nid);',
              'vars' => array(
                '0' => 'node',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#info' => array(
              'label' => 'Invio email all\'associazione ',
              'label callback' => FALSE,
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#weight' => -10,
          ),
          '2' => array(
            '#type' => 'action',
            '#settings' => array(
              'from' => '',
              'subject' => '[Sardegna Consumatore] Richiesta inviata correttamente',
              'message' => 'Gentile [author:user],
La Sua richiesta è stata correttamente inviata agli operatori delle associazioni che partecipano al progetto.  Gli operatori elaboreranno una risposta che verrà inviata all’ indirizzo email da lei registrata nel nostro portale. 
Distinti saluti
--
http://www.sardegnaconsumatore.it

',
              '#argument map' => array(
                'user' => 'author',
              ),
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'message' => array(
                    '0' => 'author',
                  ),
                ),
              ),
            ),
            '#name' => 'rules_action_mail_to_user',
            '#info' => array(
              'label' => 'Manda una mail all\'autore della richiesta',
              'label callback' => FALSE,
              'arguments' => array(
                'user' => array(
                  'type' => 'user',
                  'label' => 'Recipient',
                ),
              ),
              'module' => 'System',
              'eval input' => array(
                '0' => 'subject',
                '1' => 'message',
                '2' => 'from',
              ),
            ),
            '#weight' => -1,
          ),
          '3' => array(
            '#type' => 'action',
            '#settings' => array(
              'recipients' => array(
                '0' => 9,
              ),
              'from' => '',
              'subject' => '[SITCR] Nuova richiesta [node:title]',
              'message' => 'Gentile Operatore/Operatrice
Una nuova richiesta è stata inoltrata dall\'utente [author:user] tramite il sito SardegnaConsumatore. 
Si ricorda che il Community manager attribuirà a rotazione ad una delle quattro associazioni il compito di evadere la richiesta nell’arco di massimo 48 ore lavorative. 

Se dovesse essere attribuita alla tua associazione allora segui la procedura di risposta come da Linee guida e partecipa nel forum della tua associazione per elaborare la risposta.
Il testo della richiesta pervenuta è il seguente:
###
[node:node-body]
###
Se hai bisogno di supporto per comporre la risposta puoi contattare SardegnaIT al n° 070 6069040
--
http://www.sardegnaconsumatore.it
',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'message' => array(
                    '0' => 'node',
                    '1' => 'author',
                  ),
                  'subject' => array(
                    '0' => 'node',
                  ),
                ),
              ),
            ),
            '#name' => 'rules_action_mail_to_users_of_role',
            '#info' => array(
              'label' => 'Email Community Manager',
              'label callback' => FALSE,
              'module' => 'System',
              'eval input' => array(
                '0' => 'subject',
                '1' => 'message',
                '2' => 'from',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'segnalazione_online_rules_5' => array(
        '#type' => 'rule',
        '#set' => 'event_workflow_state_changed',
        '#label' => 'Invio email risposta',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Attribuzione automatica',
          'segnalazione_online_rules' => 'segnalazione_online_rules',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#type' => 'condition',
            '#settings' => array(
              'from_state' => array(
                '13' => '13',
                '14' => '14',
                '15' => '15',
              ),
              'to_state' => array(
                '16' => '16',
              ),
              '#argument map' => array(
                'old_state' => 'old_state',
                'new_state' => 'new_state',
              ),
            ),
            '#name' => 'workflow_check_transition',
            '#info' => array(
              'label' => 'Check workflow transition from (creation), In elaborazione, Da validare to Inviata',
              'arguments' => array(
                'old_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'Old workflow state',
                ),
                'new_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'New workflow state',
                ),
              ),
              'module' => 'Workflow',
            ),
            '#weight' => 0,
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'field' => 'field_risposta_rilevazione_refer',
              '#argument map' => array(
                'node' => 'node',
                'referenced_node' => 'referenced_node_rilevazione',
              ),
            ),
            '#name' => 'nodereference_rules_action_load',
            '#info' => array(
              'label' => 'Load a referenced node (Scheda Rilevazione)',
              'label callback' => FALSE,
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content containing the node reference field',
                ),
              ),
              'new variables' => array(
                'referenced_node_rilevazione' => array(
                  'label' => 'referenced content rilevazione',
                  'label callback' => FALSE,
                  'type' => 'node',
                ),
              ),
              'module' => 'CCK',
            ),
            '#weight' => -10,
          ),
          '1' => array(
            '#type' => 'action',
            '#settings' => array(
              'field' => 'field_scheda_problema_richie',
              '#argument map' => array(
                'node' => 'referenced_node_rilevazione',
                'referenced_node' => 'referenced_node_richiesta',
              ),
            ),
            '#name' => 'nodereference_rules_action_load',
            '#info' => array(
              'label' => 'Load a referenced node (Risposta)',
              'label callback' => FALSE,
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content containing the node reference field',
                ),
              ),
              'new variables' => array(
                'referenced_node_richiesta' => array(
                  'label' => 'referenced content richiesta',
                  'label callback' => FALSE,
                  'type' => 'node',
                ),
              ),
              'module' => 'CCK',
            ),
            '#weight' => -5,
          ),
          '2' => array(
            '#weight' => 0,
            '#type' => 'action',
            '#settings' => array(
              'to' => '[referenced_node_richiesta:author-mail]',
              'from' => '',
              'subject' => '[Sardegna Consumatore] Risposta associazione dei consumatori alla richiesta [referenced_node_richiesta:title]',
              'message' => 'Gentile [referenced_node_richiesta:author-name],
la sua richiesta [referenced_node_richiesta:title] è stata presa in esame dall’operatore [node:author-name]. Potrà visualizzare i contenuti per Lei selezionati cliccando sul seguente link:
[referenced_node_richiesta:node-url]
Potrà comunque ricevere ulteriori chiarimenti e approfondimenti contattando una delle associazioni a lei più vicina tra quelle indicate nella pagina http://www.sardegnaconsumatore.it/associazioni-registrate.
La ringraziamo per la collaborazione
Sardegna Consumatore
--
http://www.sardegnaconsumatore.it
',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'message' => array(
                    '0' => 'referenced_node_richiesta',
                    '1' => 'node',
                  ),
                  'to' => array(
                    '0' => 'referenced_node_richiesta',
                  ),
                  'subject' => array(
                    '0' => 'referenced_node_richiesta',
                  ),
                ),
              ),
            ),
            '#name' => 'rules_action_mail',
            '#info' => array(
              'label' => 'Manda una mail all\'autore della richiesta',
              'label callback' => FALSE,
              'module' => 'System',
              'eval input' => array(
                '0' => 'subject',
                '1' => 'message',
                '2' => 'from',
                '3' => 'to',
              ),
            ),
          ),
          '3' => array(
            '#info' => array(
              'label' => 'Invio segnalazione invio risposta ai membri dell\'associazione',
              'label callback' => FALSE,
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#settings' => array(
              'code' => 'gestione_richiesta_email_operatori_richiesta($referenced_node_richiesta->nid, \'risposta\');',
              'vars' => array(
                '0' => 'referenced_node_richiesta',
              ),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'segnalazione_online_rules_6' => array(
        '#type' => 'rule',
        '#set' => 'event_workflow_state_changed',
        '#label' => 'Risposta inviata',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'Attribuzione automatica',
          'ambiente_attribuzione_automatica' => 'ambiente_attribuzione_automatica',
          'segnalazione_online_rules' => 'segnalazione_online_rules',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#type' => 'condition',
            '#settings' => array(
              'type' => array(
                'risposta' => 'risposta',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#name' => 'rules_condition_content_is_type',
            '#info' => array(
              'label' => 'Updated content is Risposta',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Contenuto',
                ),
              ),
              'module' => 'Node',
            ),
            '#weight' => 0,
          ),
          '1' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Check workflow transition from Da validare to Inviata',
              'arguments' => array(
                'old_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'Old workflow state',
                ),
                'new_state' => array(
                  'type' => 'workflow_state',
                  'label' => 'New workflow state',
                ),
              ),
              'module' => 'Workflow',
            ),
            '#name' => 'workflow_check_transition',
            '#settings' => array(
              'from_state' => array(
                '15' => '15',
              ),
              'to_state' => array(
                '16' => '16',
              ),
              '#argument map' => array(
                'old_state' => 'old_state',
                'new_state' => 'new_state',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#info' => array(
              'label' => 'Execute custom PHP code',
              'module' => 'PHP',
              'eval input' => array(
                '0' => 'code',
              ),
            ),
            '#name' => 'rules_action_custom_php',
            '#settings' => array(
              'code' => 'attribuzione_automatica_complete($node->nid);',
              'vars' => array(
                '0' => 'node',
              ),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
    ),
  );
}
